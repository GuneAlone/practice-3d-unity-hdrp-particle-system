using UnityEngine;

public class ParticleController : MonoBehaviour
{
    public ParticleSystem ParticleSystem;
    public ParticleSystem ReverseParticleSystem;

    public ParticleSystem SecondParticleSystem;
    public ParticleSystem ReverseSecondParticleSystem;

    void Start()
    {
        StartCoroutine(CycleParticles());
    }

    System.Collections.IEnumerator CycleParticles()
    {
        int a = 0;
        bool b = false;

        while (true)
        {
            SetEmissionRate(ParticleSystem, 0.15f);
            SetEmissionRate(SecondParticleSystem, Random.Range(0.02f, 0.03f));

            SetEmissionRate(ReverseParticleSystem, 0f);
            SetEmissionRate(ReverseSecondParticleSystem, 0f);

            yield return new WaitForSeconds(5f);

            SetEmissionRate(ParticleSystem, 0f);
            SetEmissionRate(SecondParticleSystem, 0f);

            if (b)
            {
                SetParticleColorAndSize(ParticleSystem, new Color(0f, 142f / 255f, 213f / 255f, 94f / 255f), 5f);
                Debug.Log("After WaitForSeconds: " + Time.time);
                b = false;
            }

            yield return new WaitForSeconds(2.5f);

            SetEmissionRate(ReverseParticleSystem, 0.15f);
            SetEmissionRate(ReverseSecondParticleSystem, Random.Range(0.02f, 0.03f));

            yield return new WaitForSeconds(5f);

            SetEmissionRate(ReverseParticleSystem, 0f);
            SetEmissionRate(ReverseSecondParticleSystem, 0.0f);

            if (a % 2 == 1)
            {
                SetParticleColorAndSize(ParticleSystem, new Color(0f, 0f, 0f, 1f), 30f);
                Debug.Log("After WaitForSeconds: " + Time.time);
                b = true;
               
            }
            
            yield return new WaitForSeconds(2.5f);

            SetEmissionRate(ParticleSystem, 0.15f);
            SetEmissionRate(SecondParticleSystem, Random.Range(0.02f, 0.03f));

            a++;
        }
    }

    void SetEmissionRate(ParticleSystem particleSystem, float rate)
    {
        var emissionModule = particleSystem.emission;
        emissionModule.rateOverTime = rate;
    }

    void SetParticleColorAndSize(ParticleSystem particleSystem, Color color, float size)
    {
        var mainModule = particleSystem.main;
        mainModule.startColor = color;
        mainModule.startSize = size;
    }
}