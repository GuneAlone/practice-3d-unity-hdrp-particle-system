using System.Collections.Generic;
using UnityEngine;

public class ParticleTriggerHandler : MonoBehaviour
{
    private ParticleSystem myParticleSystem;

    void Start()
    {
        myParticleSystem = GetComponent<ParticleSystem>();

        // ��������, ��� ��������� ParticleSystem ������������
        if (myParticleSystem != null)
        {
            // �������� ������ Triggers
            var triggersModule = myParticleSystem.trigger;
            triggersModule.enabled = true;
        }
        else
        {
            Debug.LogError("Component ParticleSystem not found on the GameObject.");
        }
    }

    // ���������� ���������� �������
    void OnParticleTrigger()
    {
        List<ParticleSystem.Particle> particles = new List<ParticleSystem.Particle>(myParticleSystem.particleCount);

        // �������� �������, ��������������� ������� Enter
        int numParticles = ParticlePhysicsExtensions.GetTriggerParticles(myParticleSystem, ParticleSystemTriggerEventType.Enter, particles);

        for (int i = 0; i < numParticles; i++)
        {
            // ��� ��� ��� ��������� ������
            Debug.Log("Particle entered trigger area: " + particles[i].position);
        }

        // �������� �������� Max Particles �� 10
        ModifyMaxParticles(10);
    }

    // ������� ��� ��������� Max Particles
    void ModifyMaxParticles(int newValue)
    {
        ParticleSystem.MainModule mainModule = myParticleSystem.main;
        mainModule.maxParticles = newValue;
    }
}