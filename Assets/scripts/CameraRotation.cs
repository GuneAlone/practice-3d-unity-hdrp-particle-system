using UnityEngine;

public class CameraRotation : MonoBehaviour
{
    public float rotationSpeed = 5f;
    public float verticalRotationLimit = 90f;

    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        // ������� ������ ������ ����� ����
        transform.Rotate(Vector3.up, mouseX * rotationSpeed * Time.deltaTime);
        transform.Rotate(Vector3.left, mouseY * rotationSpeed * Time.deltaTime);

        // ������������ �������� ������ �� ���������
        Vector3 currentRotation = transform.localRotation.eulerAngles;
        float clampedRotationX = ClampAngle(currentRotation.x, -verticalRotationLimit, verticalRotationLimit);
        transform.localRotation = Quaternion.Euler(clampedRotationX, currentRotation.y, 0f);
    }

    // ����� ��� ����������� ���� �������� � �������� ���������
    float ClampAngle(float angle, float min, float max)
    {
        if (angle < 0f) angle += 360f;
        if (angle > 180f) return Mathf.Max(angle, 360f + min);
        return Mathf.Min(angle, max);
    }
}